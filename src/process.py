from defs import *
import postprocess

import subprocess
import configparser
from os import path as ospath

"""
@brief Converts a source file into a desination document file.
@param infile: string path to the file to read
@param outfile: string path to the file to write
@param srctype: pandoc file type name for infile
@param tgttype: pandoc file type name for outfile
@param highlight: pandoc syntax highlighting style name
@param extraopts: list of strings to use as additional arguments
"""
def convert(infile:str, outfile:str, srctype:str=None, tgttype:str=None, highlight:str=None, extraopts:list=None) -> None:
	target = tgttype
	if tgttype is None:
		target = typemap[ospath.splitext(tgttype)[1]]
		DEBUG("Guessed file type: " + target)
	
	args = ['pandoc'];
	if srctype is not None:
		args += ['-f', srctype]
	if tgttype is not None:
		args += ['-t', tgttype]
	if highlight is not None:
		args += ['--highlight-style', highlight]
	if extraopts is not None:
		args += extraopts
	if not tgttype in postprocess.supportedTypes:
		args += ['-s', '-o', outfile] # Produce and write a standalone document
	args.append(infile)
	
	pandoc = None
	DEBUG("executing: " + ' '.join(args))
	try:
		pandoc = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
	except subprocess.CalledProcessError as e:
		TERMINATE('pandoc', e.stderr.decode('utf-8'))
	
	if tgttype in postprocess.supportedTypes:
		DEBUG("Pandoc completed with stderr: " + pandoc.stderr.decode('utf-8'))
		postprocess.supportedTypes[tgttype](pandoc.stdout.decode('utf-8'), outfile)

"""
@brief Generates the given source file's converted output according to the config file
@param infile: path to the source document to be converted
@param configfile: path to this wiki's configuration INI
"""
def process(infile:str, configfile:str):
	config = configparser.ConfigParser()
	config.read(configfile)
	if 'formats' not in config:
		raise RuntimeError('Configuration error: required section "[formats]" not found')
	srcext = config['formats']['editextension']
	visext = config['formats']['visualextension']
	if srcext is None and visext is None:
		raise RuntimeError('Configuration error: at least one of "editextension" and "visualextension" must be specified')
	customwriter = config['formats']['customwriter']
	srctype = config['formats']['editformat']
	vistype = config['formats']['visualformat']
	if srcext is None and srctype is None and customwriter is None:
		raise RuntimeError('Configuration error: at least one of "editextension", "editformat" or "customwriter" must be specified')
	if visext is None and vistype is None and customwriter is None:
		raise RuntimeError('Configuration error: at least one of "visualextension", "visualformat", or "customwriter" must be specified')
	highlight = config['formats']['highlight']
	outfile = infile.removesuffix(srctype) + vistype
	convert(infile, outfile, srctype, vistype, highlight)
