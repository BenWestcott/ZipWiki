htmlpre='''
<html>
<head>
	<meta charset="UTF-8"/>
	<style>
		body{margin:0}
		#page{ margin:2em; text-align:justify; }
		h1{ text-align:center; border-bottom: .1em solid gray; }
		h2{ font-weight:normal; font-size:xx-large; margin-bottom:0; border-bottom: 1px solid black; }
		h3{margin-bottom:0}
		ul,ol{margin-top:0.3em}
		a{color:darkcyan}
		a:visited{color:indigo}
		details summary {cursor:pointer;}
		#nav{
			position:fixed; left:0; top:0; padding:0.7em; width:max-content; background-color:white;
			border:0.1em solid black; opacity:0.7;
		}
		#nav[open]{width:25em}
		#nav:hover,#nav:focus,#nav[open]{opacity:1}
	</style>
	<script>
	</script>
</head>
<body>
	<details id="nav">
		<summary>nav</summary>
		<div><ul><li>foo</li><li>bar</li><li>baz</li></ul></div>
	</details>
	<div id="page">'''
htmlpost='</div></body></html>'
