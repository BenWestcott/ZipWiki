from defs import *
import fragments

"""
@brief Adds basic HTML and some styling to a pandoc HTML fragment
@param frag: string of HTML
@param outfile: name of the file to write
"""
def html(frag:str, outfile:str):
	DEBUG("Postprocessing HTML fragment for file " + outfile)
	with open(outfile, "w") as o:
		o.write(fragments.htmlpre + frag + fragments.htmlpost)


supportedTypes = {
	'htm':	html,
	'html': html
}
