from os import environ

COLOR = {
	'red':		"\033[1;31m",
	'bold':		"\033[1m",
	'off':		"\033[m"
}

EXITCODES = {
	'internal':	1,
	'external': 2,
	'pandoc':	3
}

def DEBUG(msg):
	try:
		environ['DEBUG']
		print('DEBUG:', msg)
	except:
		pass

def TERMINATE(errtype:str, msg:str):
	print(COLOR['red']+'Fatal', errtype, 'error:', msg, COLOR['off'])
	exit(EXITCODES[errtype])
