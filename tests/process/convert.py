#!/bin/env python3
import testutils

import filecmp
from process import convert

# Nice case, fully specified
convert('example.wiki', 'test1.html', srctype='mediawiki', tgttype='html', highlight='pygments')
if not filecmp.cmp('test1.html', 'tc1.html', shallow=False):
	raise RuntimeError("Test case 1 failed")

# Nice case, minimally specified
convert('example.wiki', 'test2.html')
if not filecmp.cmp('test2.html', 'tc1.html', shallow=False):
	raise RuntimeError("Test case 2 failed");

print("All tests passed successfully!")
